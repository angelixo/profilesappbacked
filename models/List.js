const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let listSchema = new Schema({
  name: {
    type: String,
    lowercase:true,
  },
}, {
  collection: 'lists'
})

module.exports = mongoose.model('List', listSchema)