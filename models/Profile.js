const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let studentSchema = new Schema({
  name: {
    type: String
  },
  email: {
    type: String
  },
  phone: {
    type: Number
  },
  gender: {
      type: String,
  },
   name:{
        type: Object,
    },
    location: {
        type: Object
    },
      timezone:{
          type: Object
      },
    email: {
        type: String,
    },
    login: {
       type: Object,
    },
    dob: {
        type: Object,
    },
    registered: {
      type: Object,
    },
    phone: {
        type: String
    },
    cell: {
        type: String,
    },
    picture: {
      type: Object,
    },
    nat: {
        type: String,
    },
    list : {
      type: Object,
    }
}, {
  collection: 'students'
})

module.exports = mongoose.model('Student', studentSchema)