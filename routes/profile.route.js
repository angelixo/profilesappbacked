const express = require('express');
const profileRoute = express.Router();

// Profile model
let ProfileModel = require('../models/Profile.js');

profileRoute.route('/').get((req, res) => {
    ProfileModel.find((error, data) => {
     if (error) {
       return next(error)
     } else {
       res.json(data)
     }
   })
 })

 profileRoute.route('/create-profile').post((req, res, next) => {
    ProfileModel.create(req.body, (error, data) => {
    if (error) {
      return next(error)
    } else {
      res.json(data)
    }
  })
});

profileRoute.route('/edit-profile/:id').get((req, res) => {
   ProfileModel.findById(req.params.id, (error, data) => {
    if (error) {
      return next(error)
    } else {
      res.json(data)
    }
  })
})

// Update profile
profileRoute.route('/update-profile/:id').post((req, res, next) => {
  ProfileModel.findByIdAndUpdate(req.params.id, {
    $set: req.body
  }, (error, data) => {
    if (error) {
      return next(error);
    } else {
      res.json(data)
      console.log('Profile successfully updated!')
    }
  })
})

// Delete profile
profileRoute.route('/delete-profile/:id').delete((req, res, next) => {
  ProfileModel.findByIdAndRemove(req.params.id, (error, data) => {
    if (error) {
      return next(error);
    } else {
      res.status(200).json({
        msg: data
      })
    }
  })
})

module.exports = profileRoute;