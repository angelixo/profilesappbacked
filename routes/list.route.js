const express = require('express');
const listRoute = express.Router();

// List model
let ListModel = require('../models/List.js');

listRoute.route('/').get((req, res) => {
    ListModel.find((error, data) => {
     if (error) {
       return next(error)
     } else {
       res.json(data)
     }
   })
 })

 listRoute.route('/create-list').post((req, res, next) => {
    ListModel.create(req.body, (error, data) => {
    if (error) {
      return next(error)
    } else {
      res.json(data)
    }
  })
});

listRoute.route('/edit-list/:id').get((req, res) => {
   ListModel.findById(req.params.id, (error, data) => {
    if (error) {
      return next(error)
    } else {
      res.json(data)
    }
  })
})

// Detail List based on name
listRoute.route('/:name').get((req,res)=> {
  ListModel.findOne({ name: req.params.name }, (error, data) => {
    if (error) {
      return next(error)
    } else {
      res.json(data)
    }
  });
})

// Update list
listRoute.route('/update-list/:id').post((req, res, next) => {
  ListModel.findByIdAndUpdate(req.params.id, {
    $set: req.body
  }, (error, data) => {
    if (error) {
      return next(error);
    } else {
      res.json(data)
      console.log('List successfully updated!')
    }
  })
})

// Delete list
listRoute.route('/delete-list/:id').delete((req, res, next) => {
  ListModel.findByIdAndRemove(req.params.id, (error, data) => {
    if (error) {
      return next(error);
    } else {
      res.status(200).json({
        msg: data
      })
    }
  })
})

module.exports = listRoute;